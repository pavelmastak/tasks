<?php
session_start();
chdir(dirname(__DIR__));
require 'vendor/autoload.php';

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals($_SERVER, $_GET, $_POST, $_COOKIE, $_FILES);

$routerContainer = new Aura\Router\RouterContainer();
$map = $routerContainer->getMap();

require 'config/routes.php';

$matcher = $routerContainer->getMatcher();

$route = $matcher->match($request);
if (!$route) {
    echo "No route found for the request.";
    exit;
}

foreach ($route->attributes as $key => $val) {
    $request = $request->withAttribute($key, $val);
}

$handler = $route->handler;
$action = is_string($handler) ? new $handler() : $handler;
$response = $action($request);

foreach ($response->getHeaders() as $name => $values) {
    foreach ($values as $value) {
        header(sprintf('%s: %s', $name, $value), false);
    }
}
http_response_code($response->getStatusCode());
echo $response->getBody();