<!doctype html>
<html lang="en">
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
<?php require 'views/blocks/navbar.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <?php if (count($errors)) { ?>
                <div class="alert alert-danger" role="alert">
                    <ul>
                        <?php foreach ($errors as $error) { ?>
                            <li><?= $error ?></li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <div>
                <form action="" method="post">
                    <input type="text" id="login" name="login" class="form-control mb-4" placeholder="Login"
                           value="<?= $login ?>" autofocus>
                    <input type="password" id="password" name="password" class="form-control mb-4"
                           placeholder="Password">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>