<!doctype html>
<html lang="en">
<head>
    <title>Tasks</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<?php require 'views/blocks/navbar.php';

use App\Models\TaskRepository; ?>
<div class="container">
    <?php if (isset($_SESSION['success'])) { ?>
        <div class="alert alert-success" role="alert"><?= $_SESSION['success'] ?></div>
        <?php
        unset($_SESSION['success']);
    } ?>
    <h1>Tasks 77</h1>
    <div class="card-deck mb-3">
        <a class="btn btn-primary" href="/task/create">Create task 4</a>
    </div>
    <div class="card-deck mb-3">
        <a class="btn btn-primary" href="/task/delete">Delete task now 5</a>
    </div>
    <div class="card-deck mb-3">
        <a class="btn btn-primary" href="/task/clear">Clear all 2</a>
    </div>
    <div class="card-deck mb-3">
        <a class="btn btn-primary" href="/task/exit">Exit 2</a>
    </div>
    <div class="card-deck mb-3">
        <table id="tasks" class="table table-bordered" cellspacing="0" style="width:100%">
            <thead>
            <tr>
                <th scope="col">Username <?= $pager->getSortColumnLink('username') ?></th>
                <th scope="col">Email <?= $pager->getSortColumnLink('email') ?></th>
                <th scope="col">Text</th>
                <th scope="col">Status <?= $pager->getSortColumnLink('status') ?></th>
                <?php if (isset($_SESSION['user'])) { ?>
                    <th scope="col">Actions</th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php if (count($tasks)) {
                foreach ($tasks as $task) { ?>
                    <tr>
                        <td><?= $task['username'] ?></td>
                        <td><?= $task['email'] ?></td>
                        <td><?= $task['text'] ?><?php if ($task['edited_by_admin']) { ?>
                                <br><span class="badge badge-info">Edited by Admin</span>
                            <?php } ?></td>
                        <td><?php if (TaskRepository::isStatusCompleted($task['status'])) { ?>
                                <span class="badge badge-success">Completed</span>
                            <?php } ?>
                        </td>
                        <?php if (isset($_SESSION['user'])) { ?>
                            <td><a class="btn btn-success btn-sm" href="/task/<?= $task['id'] ?>/edit">Edit</a></td>
                        <?php } ?>
                    </tr>
                <?php }
            } else { ?>
                <tr>
                    <td colspan="5" class="text-center">No tasks</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?= $pager->render(); ?>
    </div>
</div>
</body>
<script src="https://kit.fontawesome.com/0a7f5564a0.js"></script>
</html>