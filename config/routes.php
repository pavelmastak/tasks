<?php

use App\Http\Action;

$map->get('index', '/', Action\IndexAction::class);
$map->get('task.page', '/page/{page}', Action\IndexAction::class);
$map->get('task.create', '/task/create', Action\Task\CreateAction::class);
$map->post('task.create.post', '/task/create', Action\Task\CreateAction::class);
$map->get('task.edit', '/task/{id}/edit', Action\Task\EditAction::class);
$map->post('task.edit.post', '/task/{id}/edit', Action\Task\EditAction::class);
$map->get('login', '/login', Action\LoginAction::class);
$map->post('login.post', '/login', Action\LoginAction::class);
$map->get('logout', '/logout', Action\LogoutAction::class);