<?php

namespace App\Models;

class Pagination
{
    private $totalCount;
    private $page;
    private $perPage;
    private $sidx;
    private $sord;

    public function __construct(int $totalCount, int $page, int $perPage, string $sidx, string $sord)
    {
        $this->totalCount = $totalCount;
        $this->page = $page;
        $this->perPage = $perPage;
        $this->sidx = $sidx;
        $this->sord = $sord;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getSortColumn(): int
    {
        return $this->sidx;
    }

    public function getSortDirection(): int
    {
        return $this->sord;
    }

    public function getPagesCount(): int
    {
        return ceil($this->totalCount / $this->perPage);
    }

    public function getLimit(): int
    {
        return $this->perPage;
    }

    public function getOffset(): int
    {
        return ($this->page - 1) * $this->perPage;
    }

    public function getQuery(): string {
        return "?sidx={$this->sidx}&sord={$this->sord}";
    }

    public function render(): string
    {
        if($this->getPagesCount() < 2) return '';
        $result = '<div><ul class="pagination">' . $this->getFirstUrl();
        for ($page = 1; $page <= $this->getPagesCount(); $page++) {
            $result .= $this->getPaginationUrl($page);
        }
        $result .= $this->getLastUrl() . "</ul></div>";
        return $result;
    }

    private function getPaginationUrl(int $page): string
    {
        $active = '';
        if ($page == $this->getPage()) $active = ' active';
        $href = ($page == 1) ? "/" : "/page/$page";
        return "<li class=\"page-item{$active}\"><a class=\"page-link\" href=\"{$href}{$this->getQuery()}\">{$page}</a></li>";
    }

    private function getFirstUrl(): string
    {
        $disabled = ($this->getPage() == 1) ? ' disabled' : '';
        $previous = $this->getPage() - 1;
        $page = ($this->getPage() > 2) ? "/page/$previous" : "/";
        return "<li class=\"page-item$disabled\"><a class=\"page-link\" href=\"$page{$this->getQuery()}\"><span aria-hidden=\"true\">&laquo;</span></a></li>";
    }

    private function getLastUrl(): string
    {
        $disabled = ($this->getPage() == $this->getPagesCount()) ? ' disabled' : '';
        $next = $this->getPage() + 1;
        return "<li class=\"page-item$disabled\"><a class=\"page-link\" href=\"/page/{$next}{$this->getQuery()}\"><span aria-hidden=\"true\">&raquo;</span></a></li>";
    }

    private function getCurrentPageUrl(): string
    {
        return ($this->page == 1) ? "/" : "/page/$this->page";
    }

    public function getSortColumnLink(string $column): string
    {
        $class = "fas fa-sort";
        $sord = 'asc';
        if ($column === $this->sidx) {
            if ($this->sord === 'asc') {
                $sord = 'desc';
                $class = 'fas fa-sort-up';
            } else {
                $sord = 'asc';
                $class = 'fas fa-sort-down';
            }
        }
        return "<a href=\"{$this->getCurrentPageUrl()}?sidx=$column&sord=$sord\"><i class=\"$class\"></i></a>";
    }
}
