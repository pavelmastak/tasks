<?php

namespace App\Models;

use PDO;

class TaskRepository
{
    const STATUS_COMPLETED = "completed";
    const REQUIRED_USERNAME = "Username is required";
    const REQUIRED_EMAIL = "Email is required";
    const REQUIRED_TEXT = "Text is required";
    const INVALID_EMAIL = "Email is invalid";

    private $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function countAll(): int
    {
        return $this->pdo->query('SELECT COUNT(id) FROM tasks')->fetchColumn();
    }

    public function all(string $sidx, string $sord, int $limit, int $offset): array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM tasks ORDER BY $sidx $sord LIMIT :limit OFFSET :offset");
        $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getTaskById(int $id): array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM tasks WHERE id = :id");
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $task = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$task) return [];
        return [
            'username' => $task['username'],
            'email' => $task['email'],
            'text' => $task['text'],
            'completed' => self::isStatusCompleted($task['status']),
        ];
    }

    public function insertTask(array $fields): void
    {
        $query = "INSERT INTO `tasks` (`username`, `email`, `text`) VALUES (:username, :email, :text)";
        $params = [
            ':username' => $fields['username'],
            ':email' => $fields['email'],
            ':text' => $fields['text'],
        ];
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($params);
    }

    public function updateTask(int $id, array $fields): void
    {
        $query = "UPDATE `tasks` SET `text` = :text, `status` = :status, `edited_by_admin` = :edited_by_admin WHERE id = :id;";
        $params = [
            ':text' => $fields['text'],
            ':status' => $fields['status'],
            ':edited_by_admin' => $fields['edited_by_admin'],
            ':id' => $id,
        ];
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($params);
    }

    public function isTextEdited(int $id, string $text): int
    {
        $stmt = $this->pdo->prepare("SELECT text, edited_by_admin FROM tasks WHERE id = :id");
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $task = $stmt->fetch(PDO::FETCH_ASSOC);
        if ((int)$task['edited_by_admin'] == 1) return 1;
        return ($text !== $task['text']) ? 1 : 0;
    }

    public function checkTask(array $task): array
    {
        $errors = [];
        if (!$task['username']) $errors[] = self::REQUIRED_USERNAME;
        if (!$task['email']) {
            $errors[] = self::REQUIRED_EMAIL;
        } else {
            if (!filter_var($task['email'], FILTER_VALIDATE_EMAIL)) {
                $errors[] = self::INVALID_EMAIL;
            }
        }
        if (!$task['text']) $errors[] = self::REQUIRED_TEXT;
        return $errors;
    }

    public function checkText(string $text): array
    {
        $errors = [];
        if (!$text) $errors[] = self::REQUIRED_TEXT;
        return $errors;
    }

    static function isStatusCompleted($status): bool
    {
        return ($status === self::STATUS_COMPLETED);
    }
}
