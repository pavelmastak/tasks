<?php

namespace App\Http\Action;

use PDO;
use Zend\Diactoros\Response;

class Action
{
    public $response;
    public $pdo;

    public function __construct()
    {
        $this->response = new Response();
        $dbConfig = require 'config/db.php';
        $this->pdo = new PDO(
            "{$dbConfig['driver']}:host={$dbConfig['host']};port={$dbConfig['port']};dbname={$dbConfig['database']}",
            $dbConfig['username'],
            $dbConfig['password'],
            $dbConfig['options']
        );
    }

    public function view($view, array $params = []): Response
    {
        $this->response->getBody()->write($this->render($view, $params));
        return $this->response;
    }

    public function render($view, array $params = []): string
    {
        $templateFile = "views/{$view}.php";
        extract($params, EXTR_OVERWRITE);
        ob_start();
        require $templateFile;
        return ob_get_clean();
    }
}