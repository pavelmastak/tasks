<?php

namespace App\Http\Action;

use Zend\Diactoros\ServerRequest;

class LogoutAction extends Action
{
    public function __invoke(ServerRequest $request)
    {
        unset($_SESSION['user']);
        header('Location: /');
        exit();
    }
}