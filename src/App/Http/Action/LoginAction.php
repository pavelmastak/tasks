<?php

namespace App\Http\Action;

use App\Helpers\StringHelper;
use Zend\Diactoros\ServerRequest;

class LoginAction extends Action
{
    public function __invoke(ServerRequest $request)
    {
        $errors = [];
        $login = '';

        if ($request->getMethod() === 'POST') {
            $login = StringHelper::purifier($request->getParsedBody()['login']);
            $password = trim($request->getParsedBody()['password']);

            if (!$login) $errors[] = 'Login is required';
            if (!$password) $errors[] = 'Password is required';

            if (($login && $password) && ($login !== 'admin' || $password !== '123')) {
                $errors[] = 'Invalid login or password';
            }

            if (!$errors) {
                $_SESSION['user'] = 'admin';
                header('Location: /');
                exit();
            }
        }

        return $this->view('login', compact('login', 'errors'));
    }
}
