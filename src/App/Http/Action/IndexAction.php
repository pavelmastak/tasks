<?php

namespace App\Http\Action;

use App\Models\Pagination;
use App\Models\TaskRepository;
use Zend\Diactoros\ServerRequest;

class IndexAction extends Action
{
    private const PER_PAGE = 3;
    private $tasks;

    public function __construct()
    {
        parent::__construct();
        $this->tasks = new TaskRepository($this->pdo);
    }

    public function __invoke(ServerRequest $request)
    {
        $sidx = (in_array($request->getQueryParams()['sidx'], ['username', 'email', 'status']))
            ? $request->getQueryParams()['sidx'] : 'id';
        $sord = (in_array($request->getQueryParams()['sord'], ['asc', 'desc']))
            ? $request->getQueryParams()['sord'] : '';

        $pager = new Pagination(
            $this->tasks->countAll(),
            $request->getAttribute('page') ?: 1,
            self::PER_PAGE,
            $sidx,
            $sord
        );

        $tasks = $this->tasks->all($sidx, $sord, $pager->getLimit(), $pager->getOffset());

        return $this->view('index', compact('tasks', 'pager'));
    }
}
