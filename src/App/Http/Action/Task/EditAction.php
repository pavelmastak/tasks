<?php

namespace App\Http\Action\Task;

use App\Models\TaskRepository;
use App\Http\Action\Action;
use App\Helpers\StringHelper;
use Zend\Diactoros\ServerRequest;

class EditAction extends Action
{
    private $tasks;

    public function __construct()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] !== 'admin') {
            header('Location: /login');
            exit();
        }

        parent::__construct();
        $this->tasks = new TaskRepository($this->pdo);
    }

    public function __invoke(ServerRequest $request)
    {
        $id = (int)$request->getAttribute('id');

        $errors = [];

        if ($request->getMethod() === 'POST') {
            $task = [
                'text' => StringHelper::purifier($request->getParsedBody()['text']),
                'status' => ($request->getParsedBody()['completed']) ? $this->tasks::STATUS_COMPLETED : null,
            ];

            $errors = $this->tasks->checkText($task['text']);

            $task['edited_by_admin'] = $this->tasks->isTextEdited($id, $task['text']);
            if (!$errors) $this->tasks->updateTask($id, $task);
        }

        $task = $this->tasks->getTaskById($id);

        if (!$task) {
            header('Location: /');
            exit();
        }

        return $this->view('task.edit', compact('task', 'errors'));
    }
}
