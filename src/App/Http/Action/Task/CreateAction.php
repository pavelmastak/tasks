<?php

namespace App\Http\Action\Task;

use App\Models\TaskRepository;
use App\Http\Action\Action;
use App\Helpers\StringHelper;
use Zend\Diactoros\ServerRequest;

class CreateAction extends Action
{
    private $tasks;

    public function __construct()
    {
        parent::__construct();
        $this->tasks = new TaskRepository($this->pdo);
    }

    public function __invoke(ServerRequest $request)
    {
        $errors = [];

        $task = [
            'username' => '',
            'email' => '',
            'text' => '',
        ];

        if ($request->getMethod() === 'POST') {
            $task = [
                'username' => StringHelper::purifier($request->getParsedBody()['username']),
                'email' => StringHelper::purifier($request->getParsedBody()['email']),
                'text' => StringHelper::purifier($request->getParsedBody()['text']),
            ];

            $errors = $this->tasks->checkTask($task);

            if (!$errors) {
                $this->tasks->insertTask($task);
                $_SESSION['success'] = 'Task successfully created';
                header('Location: /');
                exit();
            }
        }

        return $this->view('task.create', compact('task', 'errors'));
    }
}
