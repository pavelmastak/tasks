<?php

namespace App\Helpers;

class StringHelper
{
    static function purifier(string $text): string
    {
        return htmlspecialchars(trim($text), ENT_QUOTES | ENT_SUBSTITUTE);
    }
}